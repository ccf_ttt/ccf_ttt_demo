CREATE TABLE BOOKS (
    id INT NOT NULL AUTO_INCREMENT,
    title varchar(200),
    author varchar(200),
    PRIMARY KEY (id)
 );