exports.runQuery = function (sqlQuery) {
    return new Promise(function (resolve, reject) {
        setupDatabaseConnection('localhost', 'root', 'mysql', 'LIBRARY')
        connection.connect();
        connection.query(sqlQuery, function (error, results, fields) {
            if (error) reject(error);
            resolve(results)
        });
        connection.end();
    });
};

function setupDatabaseConnection(hostname, username, password, databaseName) {
    var mysql = require('mysql');
    connection = mysql.createConnection({
        host: hostname,
        user: username,
        password: password,
        database: databaseName
    });
    return connection;
}