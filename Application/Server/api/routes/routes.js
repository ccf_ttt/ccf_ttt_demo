module.exports = function (app) {
  var booksController = require('../controllers/BookController');

  app.route('/books')
    .get(booksController.getBooks);

  app.route('/book')
    .post(booksController.addBook)

  app.route('/book/:id')
    .delete(booksController.deleteBook)
    .put(booksController.updateBook);
};