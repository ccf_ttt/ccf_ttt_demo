const databaseUtilities = require('../database/DatabaseUtility');

/**
 * This controller handles GET requests
 */
exports.getBooks = function (req, res) {
  console.log('Received GET request for /books', req.body)
  const query = 'SELECT id, title, author from BOOKS';
  databaseUtilities.runQuery(query)
    .then(function (results) {
      console.log(results)
      res.send(results)
    });
};

/**
 * This controller handles POST requests
 */
exports.addBook = function (req, res) {
  console.log('Received POST request for book', req.body)
  const title = req.body.title;
  const author = req.body.author;
  const query = `INSERT INTO BOOKS (title, author) VALUES ('${title}','${author}')`;
  databaseUtilities.runQuery(query)
    .then(function (results) {
      console.log(results)
      res.send(results)
    });
};

/**
 * This controller handles DELETE requests
 */
exports.deleteBook = function (req, res) {
  console.log('Received DELETE request for book', req.params.id);

  const id = req.params.id;
  const query = `DELETE FROM BOOKS WHERE ID = '${id}'`;
  databaseUtilities.runQuery(query)
    .then(function (results) {
      console.log(results)
      res.send(results)
    });
};

/**
 * This controller handles PUT requests
 */
exports.updateBook = function (req, res) {
  console.log('Received UPDATE request for book', req.params.id, req.body);

  const id = req.params.id;
  const title = req.body.title;
  const author = req.body.author;
  const query = `UPDATE BOOKS SET TITLE = '${title}', AUTHOR = '${author}' WHERE ID = '${id}'`;
  databaseUtilities.runQuery(query)
    .then(function (results) {
      console.log(results)
      res.send(results)
    });
};