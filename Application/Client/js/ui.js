function renderBookData(books) {   
    books.forEach(book => {
        addBookRow(book)
    });

    console.log(books)
}

function addBookRow(book) {    
    var rowNode = document.createElement("tr");

    var idNode = document.createElement("td");
    idNode.appendChild(document.createTextNode(book.id));

    var titleNode = document.createElement("td");
    titleNode.appendChild(document.createTextNode(book.title));

    var authorNode = document.createElement("td");
    authorNode.appendChild(document.createTextNode(book.author));

    rowNode.appendChild(idNode);
    rowNode.appendChild(titleNode);
    rowNode.appendChild(authorNode);
    
    document.getElementById("books-table").appendChild(rowNode)    
}