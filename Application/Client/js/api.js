function getBooks() {
    const getBooksApiUrl = "http://localhost:9000/books"

    return fetch(getBooksApiUrl, {
        method: 'GET'
    }).then(function (response) {
        return response.json()
    });
}

function addBook() {
    const addBooksApiUrl = "http://localhost:9000/book"

    const title = document.getElementById('book-title').value;
    const author = document.getElementById('book-author').value;

    return fetch(addBooksApiUrl, {
        method: 'POST',
        body: JSON.stringify({
            title,
            author
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
}

function deleteBook() {  
    const id = document.getElementById('book-id').value;
    const deleteBooksApiUrl = `http://localhost:9000/book/${id}`

    return fetch(deleteBooksApiUrl, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
}

function updateBook() {  
    const id = document.getElementById('book-id').value;
    const title = document.getElementById('book-title').value;
    const author = document.getElementById('book-author').value;

    const updateBooksApiUrl = `http://localhost:9000/book/${id}`

    return fetch(updateBooksApiUrl, {
        method: 'PUT',
        body: JSON.stringify({
            title,
            author
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
}